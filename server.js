"use strict";
exports.__esModule = true;
var express_1 = require("express");
var path_1 = require("path");
var server = (0, express_1)();
server.use(express_1.static(__dirname + '/public'));
server.get('/', function (_req, res) {
    res.sendFile(path_1.resolve(__dirname, './src/index.html'));
});
server.get('/projects', function (_req, res) {
    res.sendFile(path_1.resolve(__dirname, './pages/projects.html'));
});
server.get('/about_me', function (_req, res) {
    res.sendFile(path_1.resolve(__dirname, './pages/about_me.html'));
});
server.get('/contact_me', function (_req, res) {
    res.sendFile(path_1.resolve(__dirname, './pages/contact_me.html'));
});
server.listen(3000, function () {
    console.log("Listening on port 3000");
});
