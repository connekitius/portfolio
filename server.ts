import express from 'express';
import path from 'path';

const server = express()
server.use(express.static(__dirname + '/public'));

server.get('/', (_req, res) => {
    res.sendFile(path.resolve(__dirname, './src/index.html'))
})

server.get('/projects', (_req, res) => {
    res.sendFile(path.resolve(__dirname, './pages/projects.html'))
})

server.get('/about_me', (_req, res) => {
    res.sendFile(path.resolve(__dirname, './pages/about_me.html'))
})

server.get('/contact_me', (_req, res) => {
    res.sendFile(path.resolve(__dirname, './pages/contact_me.html'))
})

server.listen(3000, () => {
    console.log(`Listening on port 3000`)
})