module.exports = {
  content: [    
    "./src/index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
    "./public/**/*"  
  ],
  theme: {
    extend: {
      fontFamily: {
        source_code_pro: ['Source Code Pro', 'monospace'],
        roboto_mono: ['Roboto Mono', 'monospace']
      }
    },
  },
  plugins: [],
}
